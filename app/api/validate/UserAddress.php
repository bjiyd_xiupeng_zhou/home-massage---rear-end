<?php

namespace app\api\validate;

use think\Validate;

class UserAddress extends Validate
{
    protected $rule = [
        'contacts_tel' => 'require|mobile',
        'code'         => 'require',
        'address'      => 'require',
        'house_number' => 'require',
        'contacts'     => 'require',
        'lng'          => 'require',
        'lat'          => 'require',
    ];

    protected $message = [
        'contacts_tel.require' => '手机号不能为空',
        'contacts_tel.mobile'  => '手机号格式错误',
        'code.require'         => '验证码不能为空',
        'address.require'      => '地址不能为空',
        'house_number.require' => '门牌号不能为空',
        'contacts.require'     => '联系人不能为空',
        'lng.require'          => '经度不能为空',
        'lat.require'          => '纬度不能为空',
    ];
}