<?php

namespace app\api\validate;

use think\Validate;

class Technician extends Validate
{

    protected $rule = [
        'type'              => 'require|between:1,2',
        'full_name'         => 'require',
        'sex'               => 'require|between:1,2',
        'tel'               => 'require',
        'code'              => 'require',
        'age'               => 'require',
        'intention_city_id' => 'require',
        'person_imgs'       => 'require',
        'template_code'     => 'require',
    ];

    /**
     * 验证场景
     */
    protected $scene = [
        'type.require'              => '技师身份为必填项！',
        'type.between:1,2'          => '技师身份 1保健师 2调理师',
        'full_name.require'         => '技师姓名为必填项！',
        'sex.require'               => '性别为必填项！',
        'sex.between:1,2'           => '性别 1男 2女',
        'tel.require'               => '手机号为必填项！',
        'code.require'              => '短信验证码为必填项！',
        'age.require'               => '年龄为必填项！',
        'intention_city_id.require' => '意向合作城市为必填项！',
        'person_imgs.require'       => '本人近期照为必填项！',
        'template_code.require'     => 'template_code为必填项！',
    ];


}