<?php
// 事件定义文件
return [
    'bind' => [
    ],

    'listen' => [
        'AppInit'  => [],
        'HttpRun'  => [],
        'HttpEnd'  => [],
        'LogLevel' => [],
        'LogWrite' => [],
        "couponGifts"   =>  ['\app\api\listener\CouponGiftsListener'],  // 赠送优惠券
    ],

    'subscribe' => [
    ],
];
