<?php

namespace app\api\controller;

use ba\Tree;
use Throwable;
use think\facade\Db;
use think\facade\Config;
use app\common\controller\Frontend;
use app\common\library\token\TokenExpirationException;
use app\ExceptionHandle;

class Join extends Frontend
{
    protected array $noNeedLogin = ['*'];

    public function initialize (): void
    {
        parent::initialize();
    }

    /**
     * 技师下的服务
     */
    public function techniciaToService (): void
    {
        $model = new \app\admin\model\technician\Service();
        // 查询技师可以有什么服务
        $params = $this->request->param();
        // halt(1);
        if (empty($params['technician_id'])) {
            $this->success("请传入技师信息");
        }
        $rows = $model->where([
            'technician_id' => $params['technician_id']
        ])->with(['service'])->select();
        foreach ($rows as &$row) {
            $row->service->service_cover_img = IMAGE_URL . $row->service->service_cover_img;
        }
        $this->success("", $rows);
    }

    /**
     * 服务下的技师
     */
    public function serviceToTechnicia (): void
    {
        $model = new \app\admin\model\technician\Service();
        // 查询技师可以有什么服务
        $params = $this->request->param();
        // halt(1);
        if (empty($params['service_id'])) {
            $this->success("请传入服务信息");
        }
        $rows = $model->where([
            'service_id' => $params['service_id']
        ])->with(['technician'])->select();
        $rows->hidden(['technician.password', 'technician.salt']);
        $this->success("", $rows);
    }


}