<?php

namespace app\api\controller;

use ba\Tree;
use Throwable;
use think\facade\Db;
use think\facade\Config;
use app\common\controller\Frontend;
use app\common\library\token\TokenExpirationException;

class Home extends Frontend
{
    protected array $noNeedLogin = ['*'];

    public function initialize (): void
    {
        parent::initialize();
    }

    /**
     * 前台和会员中心的初始化请求
     * @throws Throwable
     */
    public function index (): void
    {
        $classModel = new \app\admin\model\service\Classification;;
        // 因为涉及到的项目类型不多 所以直接全部查询

        $info = $classModel
            ->with(['service'])
            ->select()->each(function ($item) {
                $item->service->each(function ($service) {
                    $service->service_cover_img = IMAGE_URL . $service->service_cover_img;
                });
            });

        $this->success("", $info);

    }

    /**
     * 轮播图
     */
    public function banner ()
    {
        $bannerModel = new \app\admin\model\service\Banner;
        $info = $bannerModel->select();
        if (!empty($info)) {
            $info->each(function ($item) {
                $item->image = IMAGE_URL . $item->image;
                return $item;
            });
        }
        $this->success("", $info);
    }


}