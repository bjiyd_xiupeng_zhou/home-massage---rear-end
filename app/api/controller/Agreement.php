<?php

namespace app\api\controller;

use app\common\controller\Frontend;

class Agreement extends Frontend
{
    protected array $noNeedLogin = ['*'];
    protected array $noNeedPermission = ['*'];

    /**
     * 隐私政策
     * @return void
     * @throws \Throwable
     */
    public function PrivacyPolicy (): void
    {
        $this->success("", [
            'title'   => "隐私政策",
            'content' => get_sys_config('PrivacyPolicy')
        ]);
    }

    /**
     * 服务协议
     * @return void
     * @throws \Throwable
     */
    public function ServiceAgreement (): void
    {
        $this->success("", [
            'title'   => "服务协议",
            'content' => get_sys_config('ServiceAgreement')
        ]);
    }


    /**
     * 平台客服联系电话
     * @return void
     * @throws \Throwable
     */
    public function customer_tel (): void
    {
        $this->success("", [
            'title'   => "平台客服联系电话",
            'content' => get_sys_config('customer_tel')
        ]);
    }
}