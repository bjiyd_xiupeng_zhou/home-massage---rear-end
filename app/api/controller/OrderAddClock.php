<?php

namespace app\api\controller;

use app\common\controller\Frontend;
use ba\PayLib;
use think\facade\Db;
use think\facade\Log;
use Yansongda\Pay\Pay;

class OrderAddClock extends Frontend
{
    protected array $noNeedPermission = ['*'];

    /**
     * 创建订单
     */
    public function create (): void
    {
        $params = $this->request->post();
        Log::info('加钟订单创建参数', $params);

        $order_id = $params['order_id'];
        $service_num = 1; #加钟数量

        $orderInfo = \app\api\model\Order::where('id', $order_id)->find();
        if (!$orderInfo) {
            $this->error('订单不存在');
        }

        $addClockOrderState = \app\api\model\Order::where('association_order_sn', $orderInfo->order_sn)->value('order_state');
        Log::info('加钟订单状态' . $addClockOrderState);
        $state = ['Canceled', 'Completed'];

        if (!empty($addClockOrderState) && !in_array($addClockOrderState, $state)) {
            $this->error('已有加钟订单未完成,请勿重复创建！');
        }

        #订单必须在进行中 而且 加钟时间不能和技师预约时间冲突
        if ($orderInfo->order_state != 'Start' || $this->isTimeConflict($orderInfo, $service_num)) {
            Log::info('订单状态为服务中才能加钟或技师时间已被预约');
            $this->error('技师时间已被预约');
        }

        $orderModel = new  \app\api\model\Order();

        #加钟开始时间
        $service_start_time = date("Y-m-d H:i:s", $this->serviceEndTime($orderInfo));
        #加钟价格
        $service_price = get_sys_config('add_clock_price');

        $order_sn = 'JZ' . date("YmdHis", time()) . time() . rand(1000, 9999);
        $save = [];
        $save['service_id'] = $orderInfo->service_id;
        $save['user_id'] = $this->auth->id;
        $save['service_start_time'] = $service_start_time;
        $save['service_price'] = $service_price;
        $save['service_num'] = $service_num ?? 1;
        $save['service_duration'] = get_sys_config('ageing'); #加钟时间 后台设置
        $save['service_total_price'] = bcmul($service_price, $service_num, 2);
        $save['order_sn'] = $order_sn;
        $save['create_time'] = date("Y-m-d H:i:s", time());
        $save['order_state'] = 'ToBePaid';
        $save['technician_id'] = $orderInfo->technician_id;
        $save['address_username'] = $orderInfo->address_username;
        $save['address_tel'] = $orderInfo->address_tel;
        $save['address_house'] = $orderInfo->address_house;
        $save['address_content'] = $orderInfo->address_content;
        $save['technician_address'] = $orderInfo->technician_address;
        $save['technician_tel'] = $orderInfo->technician_tel;
        $save['remarks'] = "";
        $save['type'] = 2;
        $save['association_order_sn'] = $orderInfo->order_sn;

        $create = $orderModel->create($save);


        if (!$create) {
            $this->error("加钟订单创建失败");
        }

        #写入时间预约
        Db::name('order_reservation')->insert([
            'user_id'          => $this->auth->id,
            'technician_id'    => $orderInfo->technician_id,
            'reservation_time' => $service_start_time,
            'order_sn'         => $order_sn,
            'create_time'      => date('Y-m-d H:i:s', time()),
        ]);

        $service = Db::table("ba_service")->where([
            'service_id' => $orderInfo->service_id,
        ])->find();
        if (isset($params['pay_type']) && $params['pay_type'] == 'balance') {
            $this->success("订单创建成功", ['order_sn' => $order_sn]);
        }

        #微信支付 JSApi支付
        $order = [
            'out_trade_no' => $order_sn,
            'description'  => $service['service_name'],
            'amount'       => [
                'total' => $service_price * 100, //分
            ],
            'payer'        => [
                'openid' => $this->openid(),
            ],
            'attach'       => 'orderPayCallback'   #扩展信息
        ];
        $result = Pay::wechat(PayLib::getConfig())->mp($order);
        Log::info('创建订单 支付返回信息' . json_encode($result));
        $this->success("订单创建成功", ['order_sn' => $order_sn, 'pay_url' => $result]);
    }


    /**
     * #计算订单结束时间
     * @return float|int
     */
    protected function serviceEndTime ($orderInfo): float|int
    {
        return strtotime($orderInfo->service_start_time) + ($orderInfo->service_duration * $orderInfo->service_num * 60);
    }


    /**
     * 获取加钟规则
     * @return void
     */
    public function getAddClockRule (): void
    {
        $this->success('success', [
            'service_duration' => get_sys_config('ageing') * 60,
            'service_price'    => get_sys_config('add_clock_price')
        ]);
    }

    /**
     * 计算价格
     * @return void
     */
    public function getCountPrice (): void
    {
        $price = $this->request->param('price') ?? 0;
        $num = $this->request->param('num') ?? 0;
        $this->success('', [
            'res' => bcmul($price, $num, 2)
        ]);
    }


}