<?php

namespace app\api\controller;

use app\api\model\FareOrder;
use app\common\controller\Frontend;
use ba\PayLib;
use Yansongda\Pay\Pay;

class Fare extends Frontend
{
    protected array $noNeedPermission = ['*'];

    /**
     * 车费附加订单支付
     */
    public function pay ()
    {
        $order_id = $this->request->param('order_id');

        $orderInfo = \app\api\model\Order::where('id', $order_id)->where('user_id', $this->auth->id)->find();
        if (!$orderInfo) {
            $this->error('主订单不存在！');
        }

        $model = new FareOrder();
        $fareOrderInfo = $model->where('order_id', $order_id)->where('user_id', $this->auth->id)->find();
        if (!$fareOrderInfo) {
            $this->error('车费附加订单不存在！');
        }


        #微信支付 JSApi支付
        $order = [
            'out_trade_no' => $fareOrderInfo->fare_order_sn,
            'description'  => '附加订单-往返车费',
            'amount'       => [
                'total' => $fareOrderInfo->fare_price * 100, //分
            ],
            'payer'        => [
                'openid' => $this->openid(),
            ],
            'attach'       => 'farePayCallback'   #扩展信息
        ];
        $result = Pay::wechat(PayLib::getConfig())->mp($order);
        $this->success("订单创建成功", ['order_sn' => $fareOrderInfo->fare_order_sn, 'pay_url' => $result]);
    }


}