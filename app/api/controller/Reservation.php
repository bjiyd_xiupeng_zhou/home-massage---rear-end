<?php

namespace app\api\controller;

use app\common\controller\Frontend;
use think\facade\Db;

class Reservation extends Frontend
{
    protected array $noNeedPermission = ['*'];
    protected array $weekArr = ['周一', '周二', '周三', '周四', '周五', '周六', '周日'];
    protected array $day = ['今天', '明天'];

    public function list ()
    {
        $technician_id = $this->request->get('technician_id');
        if (!$technician_id) {
            $this->error('technician_id_参数错误');
        }

        #获取技师预约时间
        $reservation_time = Db::name('order_reservation')->where('technician_id', $technician_id)->where('reservation_time', '>', time())->column('reservation_time');


        $data = [];
        for ($i = 0; $i < 4; $i++) {
            $nj = date('n-j', strtotime("+$i day"));
            $date = date('Y-m-d', strtotime("+$i day"));
            $week = date('N', strtotime("+$i day"));
            $start_time = strtotime(date('Y-m-d 00:00:00', strtotime($date)));
            $end_time = strtotime(date('Y-m-d 23:59:59', strtotime($date)));

            $minute30 = 0;
            $data[$i]['date'] = $nj;
            $data[$i]['week'] = $this->weekArr[$week - 1];

            $k = 0;
            while (true) {
                $while_time = $start_time + $minute30;
                $start_time = $while_time;
                $minute30 = 30 * 60;

                if ($while_time > $end_time) {
                    unset($k);
                    break;
                }

                if ($while_time <= time() || $while_time - time() < $minute30 - 20 * 60) {
                    continue;
                }


                $data[$i]['list'][$k]['status'] = 'normal';
                $data[$i]['list'][$k]['date'] = date('H:i', $while_time);

                foreach ($reservation_time as $v) {
                    if ($while_time == strtotime($v)) {
                        $data[$i]['list'][$k]['status'] = 'abnormal';
                    }
                }
                $k++;
            }
        }
        $this->success('success', $data);
    }


}