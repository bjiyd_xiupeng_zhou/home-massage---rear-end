<?php

namespace app\api\controller;

use app\admin\model\stored\Value;
use app\common\controller\Frontend;
use app\common\model\UserMoneyLog;
use ba\PayLib;
use ba\Random;
use Yansongda\Pay\Pay;

class Stored extends Frontend
{
    protected array $noNeedPermission = ['*'];


    /**
     * 充值额度列表
     * @return void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function list (): void
    {
        $info = \app\admin\model\stored\value\Order::where('user_id', $this->auth->id)->where('state', 2)->find();
        $model = new Value();
        if (!empty($info)) {
            $model = $model->where('type', 1);
        }
        $list = $model->where('status', 1)->order('weight', 'desc')->select();
        foreach ($list as $v) {
            $v->deliverPrice = bcsub($v->received_amount, $v->payment_price, 2);
            $v->isDeliverPrice = 0;
            if ($v->deliverPrice > 0) {
                $v->isDeliverPrice = 1;
            }
        }
        $this->success('', $list);
    }


    /**
     * 创建储值订单
     * @return void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function createStoredOrder (): void
    {
        $stored_id = $this->request->post('stored_id');
        if (empty($stored_id)) {
            $this->error('储值额度id不能为空！');
        }
        $value = Value::where('id', $stored_id)->find();
        if (!$value) {
            $this->error('储值额度不存在！');
        }
        $order_sn = 'CZ' . date('YmdHis') . rand(1000, 9999) . time();
        $res = \app\admin\model\stored\value\Order::create([
            'stored_id'       => $stored_id,
            'user_id'         => $this->auth->id,
            'order_sn'        => $order_sn,
            'state'           => 1,
            'create_time'     => date('Y-m-d H:i:s'),
            'payment_price'   => $value->payment_price,
            'received_amount' => $value->received_amount,
        ]);
        if (!$res) {
            $this->error('订单创建失败');
        }
        $order = [
            'out_trade_no' => $order_sn,
            'description'  => '充值' . $value->payment_price . '元',
            'amount'       => [
                'total' => $value->payment_price * 100, //分
            ],
            'payer'        => [
                'openid' => $this->openid(),
            ],
            'attach'       => 'rechargePayCallback'   #扩展信息
        ];
        $result = Pay::wechat(PayLib::getConfig())->mp($order);
        $this->success("订单创建成功", ['order_sn' => $order_sn, 'pay_url' => $result]);
    }


    /**
     * 充值记录
     * @return void
     * @throws \think\db\exception\DbException
     */
    public function rechargeLog ()
    {
        $list = \app\admin\model\stored\value\Order::where('user_id', $this->auth->id)
            ->where('state', 2)
            ->field('id,order_sn,create_time,payment_price')
            ->order('create_time', 'desc')->paginate(20);
        $this->success('', [
            'list'  => $list->items(),
            'total' => $list->total(),
        ]);
    }


    /**
     * 余额变动记录
     * @return void
     * @throws \think\db\exception\DbException
     */
    public function consumptionLog (): void
    {
        $list = UserMoneyLog::where('user_id', $this->auth->id)->order('create_time', 'desc')->paginate(20);

        $this->success('', [
            'list'  => $list->items(),
            'total' => $list->total(),
        ]);
    }
}