<?php

namespace app\api\controller;

use ba\Tree;
use Throwable;
use think\facade\Db;
use think\facade\Config;
use app\common\controller\Frontend;
use app\common\library\token\TokenExpirationException;

/**
 * 服务数据
 */
class Service extends Frontend
{
    protected array $noNeedLogin = ['*'];
    protected $model;

    public function initialize(): void
    {
        parent::initialize();
        $this->model = new \app\admin\model\service\Service;
    }

    public function index(): void
    {
        $params = $this->request->param();
        $rows = $this->model->where([
            'service_id'    => $params['service_id'] 
        ])->find();
        if (!$rows) {
            $this->error("服务不存在");
        }

        $this->success("",$rows);
    }

    
}