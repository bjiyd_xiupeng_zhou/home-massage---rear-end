<?php

use ba\Filesystem;
use app\admin\library\module\Server;
use think\facade\Db;

if (!function_exists('get_account_verification_type')) {

    /**
     * 获取可用的账户验证方式
     * 用于：用户找回密码|用户注册
     * @return string[] email=电子邮件,mobile=手机短信验证
     * @throws Throwable
     */
    function get_account_verification_type (): array
    {
        $types = [];

        // 电子邮件，检查后台系统邮件配置是否全部填写
        $sysMailConfig = get_sys_config('', 'mail');
        $configured = true;
        foreach ($sysMailConfig as $item) {
            if (!$item) {
                $configured = false;
            }
        }
        if ($configured) {
            $types[] = 'email';
        }

        // 手机号，检查是否安装短信模块
        $sms = Server::getIni(Filesystem::fsFit(root_path() . 'modules/sms/'));
        if ($sms && $sms['state'] == 1) {
            $types[] = 'mobile';
        }

        return $types;
    }

    if (!function_exists('getNowServiceTimeFunc')) {

        function getNowServiceTimeFunc ($technician_id)
        {
            $day = ['今天', '明天'];
            #获取技师预约时间
            $reservation_time = Db::name('order_reservation')->where('technician_id', $technician_id)->where('reservation_time', '>', time())->column('reservation_time');

            $data = [];
            for ($i = 0; $i < 2; $i++) {
                $date = date('Y-m-d', strtotime("+$i day"));
                $start_time = strtotime(date('Y-m-d 00:00:00', strtotime($date)));
                $end_time = strtotime(date('Y-m-d 23:59:59', strtotime($date)));

                $time = time();
                $minute30 = 0;
                $k = 0;
                while (true) {
                    $while_time = $start_time + $minute30;
                    $start_time = $while_time;
                    $minute30 = 30 * 60;

                    if ($while_time > $end_time) {
                        unset($k);
                        break;
                    }

                    if ($while_time <= $time || $while_time - $time < $minute30 - 20 * 60) {
                        continue;
                    }
                    $data[$i][$k]['day'] = $day[$i];
                    $data[$i][$k]['status'] = 'normal';
                    $data[$i][$k]['date'] = date('H:i', $while_time);

                    foreach ($reservation_time as $v) {
                        if ($while_time == strtotime($v)) {
                            $data[$i][$k]['status'] = 'abnormal';
                        }
                    }
                    $k++;
                }
            }

            $normal = [];
            foreach ($data as $value) {
                foreach ($value as $k => $v) {
                    if ($v['status'] == 'normal') {
                        $info = \app\admin\model\Technician::where('technician_id', $technician_id)->find();
                        if ($k == 0) { //可服务
                            #修改技师状态
                            $info->is_service = 1;
                        } else {
                            $info->is_service = 2;
                        }//可预约
                        $info->save();
                        $v['is_service'] = $info->is_service;
                        $normal = $v;
                        break;
                    }
                }
                break;
            }

            return $normal;
        }
    }
}