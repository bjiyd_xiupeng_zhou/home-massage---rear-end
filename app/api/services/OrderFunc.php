<?php

namespace app\api\services;

use app\admin\model\Technician;
use think\facade\Db;
use think\facade\Log;
use Throwable;

class OrderFunc
{
    /**
     * 完成订单
     * @param $orderInfo
     * @return bool
     */
    public function completeService ($orderInfo): bool
    {
        $orderInfo->order_state = 'Completed'; //已完成
        $orderInfo->evaluate_state = 'ToBeEvaluated'; //待评价

        #查询技师最新的余额表更
        $technicianMoneyLog = Db::name('technician_money_log')->where('technician_id', $orderInfo->technician_id)->order('create_time', 'desc')->find();

        $technician = Technician::where('technician_id', $orderInfo->technician_id)->find();

        #开启事务
        Db::startTrans();
        try {
            if ($orderInfo->type == 2) {
                #加钟订单
                #获取系统规定的加钟提成比例
                $service_distribution_rebate = get_sys_config('add_clock_rebate') ?? 50;
                #计算技师的分成
                $distribution_rebate_decimal = bcdiv($service_distribution_rebate, "100", 2);
                $money = bcmul($orderInfo->service_reality_price, $distribution_rebate_decimal, 2);
            } else {
                #正常订单
                #服务返佣比例
                $service_distribution_rebate = \app\admin\model\service\Service::where('service_id', $orderInfo->service_id)->value('distribution_rebate') ?? 0;
                #计算技师的分成  $orderInfo->service_reality_price * $service_distribution_rebate
                $distribution_rebate = bcdiv($service_distribution_rebate, "100", 2);
                $money = bcmul($orderInfo->service_reality_price, $distribution_rebate, 2);
                #给技师加服务单数
                #现有服务单数+服务总单数
                $technician->service_order_num = $technician->service_order_num + $orderInfo->service_num;
                $technician->save();
            }
            $afterMoney = $technicianMoneyLog['after'] ?? 0;
            #更新订单状态
            $orderInfo->save();

            Db::name('technician_money_log')->insert([
                'technician_id' => $orderInfo->technician_id,
                'order_id'      => $orderInfo->id,
                'money'         => $money,
                'before'        => $afterMoney,
                'after'         => bcadd($afterMoney, $money, 2),
                'memo'          => '订单：' . $orderInfo->order_sn . '状态：完结  服务返佣比例：' . $service_distribution_rebate . '%  技师实际返佣金额：' . $money . '元',
                'create_time'   => date("Y-m-d H:i:s", time()),
                'type'          => 1
            ]);

            #修改技师表余额
            $technician->money = bcadd($technician->money, $money, 2);
            $technician->save();

            #取消技师预约
            Db::name('order_reservation')->where('technician_id', $orderInfo->technician_id)->where('reservation_time', date("Y-m-d H:i", strtotime($orderInfo->service_start_time)))->delete();

            #记录订单状态
            setOrderStateLog($orderInfo->id, 'Completed');
            Db::commit();
        } catch (Throwable $e) {
            Db::rollback();
            Log::error('订单完成失败：user_id :' . $orderInfo->user_id . ' order_id ' . $orderInfo->id . 'errMsg' . $e->getMessage());
            return false;
        }
        return true;
    }





}