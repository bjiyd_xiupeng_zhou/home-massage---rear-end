<?php

namespace app\api\model;

use app\admin\model\service\Service;
use app\admin\model\Technician;
use think\Model;

class TechnicianMoneyLog extends Model
{
    // 表名
    protected $name = 'technician_money_log';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;


}