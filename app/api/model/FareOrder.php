<?php

namespace app\api\model;

use app\admin\model\service\Service;
use app\admin\model\Technician;
use think\Model;

class FareOrder extends Model
{
    // 表名
    protected $name = 'fare_order';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;


}