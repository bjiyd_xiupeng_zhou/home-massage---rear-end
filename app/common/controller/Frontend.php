<?php

namespace app\common\controller;

use app\common\model\User;
use think\facade\Db;
use think\facade\Log;
use Throwable;
use think\facade\Event;
use app\common\library\Auth;
use think\exception\HttpResponseException;
use app\common\library\token\TokenExpirationException;

class Frontend extends Api
{
    /**
     * 无需登录的方法
     * 访问本控制器的此方法，无需会员登录
     * @var array
     */
    protected array $noNeedLogin = [];

    /**
     * 无需鉴权的方法
     * @var array
     */
    protected array $noNeedPermission = [];

    /**
     * 权限类实例
     * @var Auth
     */
    protected Auth $auth;

    /**
     * 初始化
     * @throws Throwable
     * @throws HttpResponseException
     */
    public function initialize (): void
    {
        parent::initialize();

        $needLogin = !action_in_arr($this->noNeedLogin);

        try {

            // 初始化会员鉴权实例
            $this->auth = Auth::instance();
            $token = get_auth_token(['ba', 'user', 'token']);
            Log::info('token: ' . $token);
            if ($token) $this->auth->init($token);

        } catch (TokenExpirationException) {
            if ($needLogin) {
                $this->error(__('Token expiration'), [], 409);
            }
        }

        if ($needLogin) {
            if (!$this->auth->isLogin()) {
                $this->error(__('Please login first'), [
                    'type' => $this->auth::NEED_LOGIN
                ], $this->auth::LOGIN_RESPONSE_CODE);
            }
            if (!action_in_arr($this->noNeedPermission)) {
                $routePath = ($this->app->request->controllerPath ?? '') . '/' . $this->request->action(true);
                if (!$this->auth->check($routePath)) {
                    $this->error(__('You have no permission'), [], 401);
                }
            }
        }

        // 会员验权和登录标签位
        Event::trigger('frontendInit', $this->auth);
    }

    /**
     * #判断加钟时间和技师预约时间是否冲突
     * @return bool
     */
    protected function isTimeConflict ($orderInfo, $service_num): bool
    {
        #订单结束时间
        $serviceEndTime = strtotime($orderInfo['service_start_time']) + ($orderInfo['service_duration'] * $orderInfo['service_num'] * 60);

        $addClockEndTime = $serviceEndTime + get_sys_config('ageing') * 60 * $service_num;

        return (bool)Db::name('order_reservation')->where('technician_id',$orderInfo['technician_id'])->where('reservation_time', '>', date('Y-m-d H:i:s', time()))->where('reservation_time', '<=', date('Y-m-d H:i:s', $addClockEndTime))->find();
    }

    protected function openid ()
    {
        return User::where('id', $this->auth->id)->value('open_id');
    }
}