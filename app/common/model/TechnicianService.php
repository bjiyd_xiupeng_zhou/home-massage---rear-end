<?php

namespace app\common\model;

use think\Model;

/**
 * Classification
 */
class TechnicianService extends Model
{
    // 表主键
    protected $pk = 'id';

    // 表名
    protected $name = 'technician_service';

}