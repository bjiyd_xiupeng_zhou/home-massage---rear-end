<?php
declare (strict_types=1);

namespace app\technician_api\controller;

use Throwable;
use ba\ClickCaptcha;
use think\facade\Config;
use think\facade\Validate;
use app\common\facade\Token;
use app\admin\model\AdminLog;
use app\common\controller\Technician;
use think\facade\Db;

class My extends Technician
{
    protected array $noNeedLogin      = [];
    protected array $noNeedPermission = ['*'];
    protected $model;

    public function initialize():void
    {
        parent::initialize();
        $this->model = new \app\admin\model\Order;
    }
    
    /**
     * 技师端
     * 综合评分  成交率  服务次数
     * 账户余额  本月提现金额  今日收益金额  本月收益
     */
    public function index(){
        // 综合评分
        $scoreNumber = Db::table("ba_order_evaluate")->where(['technician_id'=>$this->auth->technician_id])->sum("score");
        $count = Db::table("ba_order_evaluate")->where(['technician_id'=>$this->auth->technician_id])->count();
        $pingfen = 0;
        if($scoreNumber != 0){
            $pingfen = (int)$scoreNumber / $count;
        }

        // 成交率 
        $payOrderNumber = $this->model
        ->whereNotNull('payment_time')
        ->count();
        $payEndNumber = $this->model
        ->where([
            'order_state'=>'Completed'
        ])
        ->count();
        if($payOrderNumber == 0){
            $transactionRate = 0;
        }else{
            $transactionRate = intval(( $payEndNumber / $payOrderNumber) * 0.01);
        }
        
        // 服务次数
        $serviceNumber = $payEndNumber;

        // 账户余额
        $money = Db::table("ba_technician")->where([
            'technician_id'    =>  $this->auth->technician_id
        ])->value("money");
        
        // 本月提现金额
        $withdrawal = Db::table("ba_technician_money_log")
        ->whereMonth('create_time')
        ->where([
            'technician_id' =>  $this->auth->technician_id,
            'type'  =>  3
        ])->sum("money");

        // 今日收益
        $day = Db::table("ba_technician_money_log")
        ->whereDay('create_time')
        ->where([
            'technician_id' =>  $this->auth->technician_id,
            'type'  =>  1
        ])->sum("money");

        // 本月收益
        $month = Db::table("ba_technician_money_log")
        ->whereMonth('create_time')
        ->where([
            'technician_id' =>  $this->auth->technician_id,
            'type'  =>  1
        ])->sum("money");

        $this->success("",[
            'pingfen'   =>  $pingfen,
            'transactionRate'   =>  $transactionRate,
            'serviceNumber'   =>  $serviceNumber,
            'money'   =>  $money,
            'withdrawal'   =>  $withdrawal,
            'day'   =>  $day,
            'month'   =>  $month,
        ]);
    }

    /**
     * 立即提现
     * 银行卡信息选择
     */
    // public function 
    /**
     * 收入列表
     */ 
    public function income()
    {
        $list = Db::table("ba_technician_money_log")
        ->where([
            ["type","in","1,2,4"]
        ])
        ->order("create_time",'desc')
        ->paginate(10);
        $this->success("",$list);
    }
    /**
     * 提现列表
     */
    public function withdrawal(){
        $list = Db::table("ba_technician_money_log")
        ->where([
            ['type','=', '3']
        ])
        ->order("create_time",'desc')
        ->paginate(20);
        $this->success("",$list);
    }
    /**
     * 添加银行卡
     */
    // public function 
    /**
     * 联系客服电话
     */
    public function adminPhone()
    {
        $phone = Db::table("ba_config")
        ->where([
            ['name','=','customer_tel']
        ])
        ->value("value");
        $this->success("",[
            'phone' => $phone
        ]);
    }
    /**
     * 用户协议  隐私政策  关于我们
     */
    public function editor(){
        $name = $this->request->param("name",'PrivacyPolicy');

        $text = Db::table("ba_config")
        ->where([
            ['name','=',$name]
        ])
        ->value("value");
        $this->success("",[
            'text' => $text
        ]);
    }

    /**
     * 修改手机号 头像  出行方式
     */
    public function changeInfo()
    {
        $params = $this->request->param();
        $this->auth->getTechnician()->avatar = $params['avatar'];
        $this->auth->getTechnician()->username = $params['tel'];
        $this->auth->getTechnician()->tel = $params['tel'];
        $this->auth->getTechnician()->is_travel = $params['is_travel'];
        $this->auth->getTechnician()->save();
        $this->success("修改成功");
    }
}