<?php
declare (strict_types=1);

namespace app\technician_api\controller;

use Throwable;
use ba\ClickCaptcha;
use think\facade\Config;
use think\facade\Validate;
use app\common\facade\Token;
use app\admin\model\AdminLog;
use app\common\controller\Technician;
use think\facade\Db;

class Fareorder extends Technician
{
    protected array $noNeedLogin = [];
    protected array $noNeedPermission = ['*'];
    protected $model;

    public function initialize (): void
    {
        parent::initialize();
        $this->model = new \app\technician_api\model\Fareorder;
    }

    /**
     * 创建车费订单 -> 要在技师到达后 才能上传凭证
     */
    public function create ()
    {
        $params = $this->request->param();
        $order = \app\admin\model\Order::where(['id' => $params['order_id']])
            ->find();
        if (!$order) {
            $this->error("订单查询失败");
        }
        // if($order['order_state'] != 'Reach'){
        //     $this->error("订单状态异常");
        // }

        $data = [
            'order_id'             => $params['order_id'],
            'fare_price'           => bcmul($params['fare_price'], "2", 2),
            'fare_create_time'     => date("Y-m-d H:i:s", time()),
            'fare_fare_price_imgs' => $params['fare_fare_price_imgs'],
            'fare_order_sn'        => $this->model->orderSn(),
            'user_id'              => $order['user_id'],
            'technician_id'        => $this->auth->technician_id
        ];
        $this->model
            ->create($data);
        $this->success("提交订单成功");
    }


    private function orderSn ()
    {

    }

}