<?php
declare (strict_types=1);

namespace app\technician_api\controller;

use Throwable;
use ba\ClickCaptcha;
use think\facade\Config;
use think\facade\Validate;
use app\common\facade\Token;
use app\admin\model\AdminLog;
use app\common\controller\Technician;
use think\facade\Db;

class Card extends Technician
{
    protected array $noNeedLogin      = [];
    protected array $noNeedPermission = ['*'];
    protected $model;

    public function initialize():void
    {
        parent::initialize();
        $this->model = new \app\technician_api\model\Card;
    }

    /**
     * 技师银行卡列表
     */
    public function index()
    {
        $rows = $this->model
        ->where([   
            "technician_id" =>  $this->auth->technician_id
        ])
        ->order("create_time",'desc')
        ->select();
        $this->success("",$rows);
    }

    public function add()
    {
        $params = $this->request->param();

        $validate = \think\facade\Validate::rule([
            'name|用户名'  => 'require|max:25',
            'card_number|银行卡号' => 'require',
            'card_name|银行名称' => 'require',
            'bank|开户行' => 'require',
        ]);

        if (!$validate->check($params)) {
            $this->error($validate->getError());
        }
        $save = $this->model
        ->create([
            'name'  =>  $params['name'],
            'card_number'  =>  $params['card_number'],
            'card_name'  =>  $params['card_name'],
            'bank'  =>  $params['bank'],
            'technician_id'  =>  $this->auth->technician_id,
        ]);
        if($save){
            $this->success("保存成功");
        }
        $this->error("保存失败");
    }

    public function del()
    {
        $params = $this->request->param();
        $find = $this->model
        ->where([
            'card_id'   =>  $params['card_id'],
            'technician_id' =>  $this->auth->technician_id
        ])
        ->find();
        if(!$find){
            $this->error("数据异常");
        }
        $d = $this->model
        ->where([
            'card_id'   =>  $params['card_id'],
            'technician_id' =>  $this->auth->technician_id
        ])
        ->delete();
        if($d){
            $this->success("删除成功");
        }
        $this->error("删除失败");
    }
}