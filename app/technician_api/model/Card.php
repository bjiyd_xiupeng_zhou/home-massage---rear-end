<?php

namespace app\technician_api\model;

use think\Model;

/**
 * 银行卡表
 */
class Card extends Model
{

    // 表主键
    protected $pk = 'card_id';

    // 表名
    protected $name = 'card';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;

    public function technician(){
        return $this->belongsTo(Technician::class,'technician_id','technician_id');
    }

}