<?php

namespace app\technician_api\model;

use think\Model;

/**
 * 车费订单表
 */
class Fareorder extends Model
{

    // 表主键
    protected $pk = 'fare_id';

    // 表名
    protected $name = 'fare_order';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    public function order(){
        return $this->belongsTo(\app\admin\model\Order::class,'order_id','id');
    }


    public function orderSn ()
    {
        do {
            $order = $this->generateOrderNumber();
            $find = Fareorder::where([
                'fare_order_sn' => $order
            ])->find();
        } while ($find);
        return $order;
    }

    private function generateOrderNumber ()
    {
        $order_id_main = date('YmdHis') . rand(10000000, 99999999);
        $order_id_len = strlen($order_id_main);
        $order_id_sum = 0;
        for ($i = 0; $i < $order_id_len; $i++) {
            $order_id_sum += (int)(substr($order_id_main, $i, 1));
        }
        $osn = $order_id_main . str_pad((100 - $order_id_sum % 100) % 100, 2, '0', STR_PAD_LEFT);
        return $osn;
    }
}