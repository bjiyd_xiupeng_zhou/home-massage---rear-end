<?php

namespace app\technician_api\model;

use think\Model;
use think\model\concern\SoftDelete;

/**
 * Technician
 */
class Technician extends Model
{
    use SoftDelete;

    // 表主键
    protected $pk = 'technician_id';

    // 表名
    protected $name = 'technician';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;


    public function getBriefIntroductionAttr ($value): string
    {
        return !$value ? '' : htmlspecialchars_decode($value);
    }
}