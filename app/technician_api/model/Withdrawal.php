<?php

namespace app\technician_api\model;

use think\Model;

class Withdrawal extends Model
{

    // 表主键
    protected $pk = 'withdrawal_id';

    // 表名
    protected $name = 'withdrawal';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;

    public $status = [
        '申请中',
        '已打款',
        '已拒绝打款',
    ];
    public function getStatusTextAttr($row, $rows){
        if(empty($rows['status']) && $rows['status'] !== 0)
            return "";

        return $this->status[$rows['status']];
    }

    public function card(){
        return $this->belongsTo(Card::class, "card_id",'card_id');
    }
}