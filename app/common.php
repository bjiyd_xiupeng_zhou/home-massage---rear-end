<?php
// 应用公共文件

use think\App;
use ba\Filesystem;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\Response;
use think\facade\Db;
use think\facade\Lang;
use think\facade\Event;
use think\facade\Config;
use voku\helper\AntiXSS;
use app\admin\model\Config as configModel;
use think\exception\HttpResponseException;
use Symfony\Component\HttpFoundation\IpUtils;

if (!function_exists('__')) {

    /**
     * 语言翻译
     * @param string $name 被翻译字符
     * @param array $vars 替换字符数组
     * @param string $lang 翻译语言
     * @return mixed
     */
    function __ (string $name, array $vars = [], string $lang = ''): mixed
    {
        if (is_numeric($name) || !$name) {
            return $name;
        }
        return Lang::get($name, $vars, $lang);
    }
}

if (!function_exists('filter')) {

    /**
     * 输入过滤
     * 富文本反XSS请使用 clean_xss，也就不需要及不能再 filter 了
     * @param string $string 要过滤的字符串
     * @return string
     */
    function filter (string $string): string
    {
        // 去除字符串两端空格（对防代码注入有一定作用）
        $string = trim($string);

        // 过滤html和php标签
        $string = strip_tags($string);

        // 特殊字符转实体
        return htmlspecialchars($string, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401, 'UTF-8');
    }
}

if (!function_exists('clean_xss')) {

    /**
     * 清理XSS
     * 通常只用于富文本，比 filter 慢
     * @param string $string
     * @return string
     */
    function clean_xss (string $string): string
    {
        return (new AntiXSS())->xss_clean($string);
    }
}

if (!function_exists('htmlspecialchars_decode_improve')) {
    /**
     * html解码增强
     * 被 filter函数 内的 htmlspecialchars 编码的字符串，需要用此函数才能完全解码
     * @param string $string
     * @param int $flags
     * @return string
     */
    function htmlspecialchars_decode_improve (string $string, int $flags = ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401): string
    {
        return htmlspecialchars_decode($string, $flags);
    }
}

if (!function_exists('get_sys_config')) {

    /**
     * 获取站点的系统配置，不传递参数则获取所有配置项
     * @param string $name 变量名
     * @param string $group 变量分组，传递此参数来获取某个分组的所有配置项
     * @param bool $concise 是否开启简洁模式，简洁模式下，获取多项配置时只返回配置的键值对
     * @return mixed
     * @throws Throwable
     */
    function get_sys_config (string $name = '', string $group = '', bool $concise = true): mixed
    {
        if ($name) {
            // 直接使用->value('value')不能使用到模型的类型格式化
            $config = configModel::cache($name, null, configModel::$cacheTag)->where('name', $name)->find();
            if ($config) $config = $config['value'];
        } else {
            if ($group) {
                $temp = configModel::cache('group' . $group, null, configModel::$cacheTag)->where('group', $group)->select()->toArray();
            } else {
                $temp = configModel::cache('sys_config_all', null, configModel::$cacheTag)->order('weigh desc')->select()->toArray();
            }
            if ($concise) {
                $config = [];
                foreach ($temp as $item) {
                    $config[$item['name']] = $item['value'];
                }
            } else {
                $config = $temp;
            }
        }
        return $config;
    }
}

if (!function_exists('get_route_remark')) {

    /**
     * 获取当前路由后台菜单规则的备注信息
     * @return string
     */
    function get_route_remark (): string
    {
        $controllerName = request()->controller(true);
        $actionName = request()->action(true);
        $path = str_replace('.', '/', $controllerName);

        $remark = Db::name('admin_rule')
            ->where('name', $path)
            ->whereOr('name', $path . '/' . $actionName)
            ->value('remark');
        return __((string)$remark);
    }
}

if (!function_exists('full_url')) {

    /**
     * 获取资源完整url地址；若安装了云存储或 config/buildadmin.php 配置了CdnUrl，则自动使用对应的CdnUrl
     * @param string $relativeUrl 资源相对地址 不传入则获取域名
     * @param boolean $domain 是否携带域名 或者直接传入域名
     * @param string $default 默认值
     * @return string
     */
    function full_url (string $relativeUrl = '', bool $domain = true, string $default = ''): string
    {
        // 存储/上传资料配置
        Event::trigger('uploadConfigInit', App::getInstance());

        $cdnUrl = Config::get('buildadmin.cdn_url');
        if (!$cdnUrl) $cdnUrl = request()->upload['cdn'] ?? '//' . request()->host();
        if ($domain === true) {
            $domain = $cdnUrl;
        } elseif ($domain === false) {
            $domain = '';
        }

        $relativeUrl = $relativeUrl ?: $default;
        if (!$relativeUrl) return $domain;

        $regex = "/^((?:[a-z]+:)?\/\/|data:image\/)(.*)/i";
        if (preg_match('/^http(s)?:\/\//', $relativeUrl) || preg_match($regex, $relativeUrl) || $domain === false) {
            return $relativeUrl;
        }
        return $domain . $relativeUrl;
    }
}

if (!function_exists('encrypt_password')) {

    /**
     * 加密密码
     */
    function encrypt_password ($password, $salt = '', $encrypt = 'md5')
    {
        return $encrypt($encrypt($password) . $salt);
    }
}

if (!function_exists('str_attr_to_array')) {

    /**
     * 将字符串属性列表转为数组
     * @param string $attr 属性，一行一个，无需引号，比如：class=input-class
     * @return array
     */
    function str_attr_to_array (string $attr): array
    {
        if (!$attr) return [];
        $attr = explode("\n", trim(str_replace("\r\n", "\n", $attr)));
        $attrTemp = [];
        foreach ($attr as $item) {
            $item = explode('=', $item);
            if (isset($item[0]) && isset($item[1])) {
                $attrVal = $item[1];
                if ($item[1] === 'false' || $item[1] === 'true') {
                    $attrVal = !($item[1] === 'false');
                } elseif (is_numeric($item[1])) {
                    $attrVal = (float)$item[1];
                }
                if (strpos($item[0], '.')) {
                    $attrKey = explode('.', $item[0]);
                    if (isset($attrKey[0]) && isset($attrKey[1])) {
                        $attrTemp[$attrKey[0]][$attrKey[1]] = $attrVal;
                        continue;
                    }
                }
                $attrTemp[$item[0]] = $attrVal;
            }
        }
        return $attrTemp;
    }
}

if (!function_exists('action_in_arr')) {

    /**
     * 检测一个方法是否在传递的数组内
     * @param array $arr
     * @return bool
     */
    function action_in_arr (array $arr = []): bool
    {
        $arr = is_array($arr) ? $arr : explode(',', $arr);
        if (!$arr) {
            return false;
        }
        $arr = array_map('strtolower', $arr);
        if (in_array(strtolower(request()->action()), $arr) || in_array('*', $arr)) {
            return true;
        }
        return false;
    }
}

if (!function_exists('build_suffix_svg')) {

    /**
     * 构建文件后缀的svg图片
     * @param string $suffix 文件后缀
     * @param ?string $background 背景颜色，如：rgb(255,255,255)
     * @return string
     */
    function build_suffix_svg (string $suffix = 'file', string $background = null): string
    {
        $suffix = mb_substr(strtoupper($suffix), 0, 4);
        $total = unpack('L', hash('adler32', $suffix, true))[1];
        $hue = $total % 360;
        [$r, $g, $b] = hsv2rgb($hue / 360, 0.3, 0.9);

        $background = $background ?: "rgb($r,$g,$b)";

        return '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
            <path style="fill:#E2E5E7;" d="M128,0c-17.6,0-32,14.4-32,32v448c0,17.6,14.4,32,32,32h320c17.6,0,32-14.4,32-32V128L352,0H128z"/>
            <path style="fill:#B0B7BD;" d="M384,128h96L352,0v96C352,113.6,366.4,128,384,128z"/>
            <polygon style="fill:#CAD1D8;" points="480,224 384,128 480,128 "/>
            <path style="fill:' . $background . ';" d="M416,416c0,8.8-7.2,16-16,16H48c-8.8,0-16-7.2-16-16V256c0-8.8,7.2-16,16-16h352c8.8,0,16,7.2,16,16 V416z"/>
            <path style="fill:#CAD1D8;" d="M400,432H96v16h304c8.8,0,16-7.2,16-16v-16C416,424.8,408.8,432,400,432z"/>
            <g><text><tspan x="220" y="380" font-size="124" font-family="Verdana, Helvetica, Arial, sans-serif" fill="white" text-anchor="middle">' . $suffix . '</tspan></text></g>
        </svg>';
    }
}

if (!function_exists('get_area')) {

    /**
     * 获取省份地区数据
     * @throws Throwable
     */
    function get_area (): array
    {
        $province = request()->get('province', '');
        $city = request()->get('city', '');
        $where = ['pid' => 0, 'level' => 1];
        if ($province !== '') {
            $where['pid'] = $province;
            $where['level'] = 2;
            if ($city !== '') {
                $where['pid'] = $city;
                $where['level'] = 3;
            }
        }
        return Db::name('area')
            ->where($where)
            ->field('id as value,name as label')
            ->select()
            ->toArray();
    }
}

if (!function_exists('hsv2rgb')) {
    function hsv2rgb ($h, $s, $v): array
    {
        $r = $g = $b = 0;

        $i = floor($h * 6);
        $f = $h * 6 - $i;
        $p = $v * (1 - $s);
        $q = $v * (1 - $f * $s);
        $t = $v * (1 - (1 - $f) * $s);

        switch ($i % 6) {
            case 0:
                $r = $v;
                $g = $t;
                $b = $p;
                break;
            case 1:
                $r = $q;
                $g = $v;
                $b = $p;
                break;
            case 2:
                $r = $p;
                $g = $v;
                $b = $t;
                break;
            case 3:
                $r = $p;
                $g = $q;
                $b = $v;
                break;
            case 4:
                $r = $t;
                $g = $p;
                $b = $v;
                break;
            case 5:
                $r = $v;
                $g = $p;
                $b = $q;
                break;
        }

        return [
            floor($r * 255),
            floor($g * 255),
            floor($b * 255)
        ];
    }
}

if (!function_exists('ip_check')) {

    /**
     * IP检查
     * @throws Throwable
     */
    function ip_check ($ip = null): void
    {
        $ip = is_null($ip) ? request()->ip() : $ip;
        $noAccess = get_sys_config('no_access_ip');
        $noAccess = !$noAccess ? [] : array_filter(explode("\n", str_replace("\r\n", "\n", $noAccess)));
        if ($noAccess && IpUtils::checkIp($ip, $noAccess)) {
            $response = Response::create(['msg' => 'No permission request'], 'json', 403);
            throw new HttpResponseException($response);
        }
    }
}

if (!function_exists('set_timezone')) {

    /**
     * 设置时区
     * @throws Throwable
     */
    function set_timezone ($timezone = null): void
    {
        $defaultTimezone = Config::get('app.default_timezone');
        $timezone = is_null($timezone) ? get_sys_config('time_zone') : $timezone;
        if ($timezone && $defaultTimezone != $timezone) {
            Config::set([
                'app.default_timezone' => $timezone
            ]);
            date_default_timezone_set($timezone);
        }
    }
}

if (!function_exists('get_upload_config')) {

    /**
     * 获取上传配置
     * @return array
     */
    function get_upload_config (): array
    {
        // 存储/上传资料配置
        Event::trigger('uploadConfigInit', App::getInstance());

        $uploadConfig = Config::get('upload');
        $uploadConfig['maxsize'] = Filesystem::fileUnitToByte($uploadConfig['maxsize']);

        $upload = request()->upload;
        if (!$upload) {
            $uploadConfig['mode'] = 'local';
            return $uploadConfig;
        }
        unset($upload['cdn']);
        return array_merge($upload, $uploadConfig);
    }
}

if (!function_exists('get_auth_token')) {

    /**
     * 获取鉴权 token
     * @param array $names
     * @return string
     */
    function get_auth_token (array $names = ['ba', 'token']): string
    {
        $separators = [
            'header' => ['', '-'], // batoken、ba-token【ba_token 不在 header 的接受列表内因为兼容性不高，改用 http_ba_token】
            'param'  => ['', '-', '_'], // batoken、ba-token、ba_token
            'server' => ['_'], // http_ba_token
        ];

        $tokens = [];
        $request = request();
        foreach ($separators as $fun => $sps) {
            foreach ($sps as $sp) {
                $tokens[] = $request->$fun(($fun == 'server' ? 'http_' : '') . implode($sp, $names));
            }
        }
        $tokens = array_filter($tokens);
        return array_values($tokens)[0] ?? '';
    }
}

if (!function_exists('calculateDistance')) {

    function calculateDistance ($lat1, $lon1, $lat2, $lon2)
    {
        // 地球半径（单位：米）
        $earthRadius = 6371000;

        // 将角度转换为弧度
        $lat1 = deg2rad($lat1);
        $lon1 = deg2rad($lon2);
        $lat2 = deg2rad($lat2);
        $lon2 = deg2rad($lon2);

        // 计算经纬度差值
        $latDiff = $lat2 - $lat1;
        $lonDiff = $lon2 - $lon1;

        // 计算Haversine公式
        $a = pow(sin($latDiff / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($lonDiff / 2), 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        $distance = $earthRadius * $c;

        return $distance;
    }
}

// 手机号匹配验证
function validatePhoneNumber ($phoneNumber)
{
    $pattern = '/^1[3-9]\d{9}$/'; // 中国大陆手机号正则表达式
    if (preg_match($pattern, $phoneNumber)) {
        return true; // 手机号格式正确
    } else {
        return false; // 手机号格式错误
    }
}


/**
 * 验证手机验证码
 * @param string $code 用户输入的验证码
 * @param string $mobile 验用户手机号
 * @param string $template_code 模板code
 */
if (!function_exists('checkCaptcha')) {
    function checkCaptcha ($code, $mobile, $template_code): bool
    {
        try {
            return (new ba\Captcha)->check($code, $mobile . $template_code);
        } catch (Throwable $e) {
            \think\facade\Log::error('checkCaptcha error: ' . $e->getMessage() . ' code:' . $code . ' id:' . $id);
            return false;
        }
    }
}

/**
 * 记录订单状态
 * @param int $order_id 订单id
 * @param string $order_state 订单状态
 */
if (!function_exists('setOrderStateLog')) {
    function setOrderStateLog ($order_id, $order_state): bool
    {
        \think\facade\Log::info('setOrderStateLog order_id:' . $order_id . ' order_state:' . $order_state);
        $ret = Db::name('order_state_timeline')->insert([
            'order_id'    => $order_id,
            'order_state' => $order_state,
            'create_time' => date('Y-m-d H:i:s', time())]);
        if (!$ret) return false;
        return true;
    }
}


/**
 *发送短信
 */
if (!function_exists('sms_send')) {

    function sms_send ($mobile, $template_code): bool
    {
        $code = rand(1000, 9999);
        $time = date("Y-m-d H:i:s", time());
        $template_code_info = [
            'reset_pay_password'        => "【貂蝉到家】您正在进行修改支付密码操作, 验证码为{$code},请勿泄漏于他人。",
            'technician_login'          => "【貂蝉到家】您正在进行技师登录操作, 验证码为{$code},请勿泄漏于他人。",
            'technician_recruitment'    => "【貂蝉到家】您正在进行技师入驻操作, 验证码为{$code},请勿泄漏于他人。",
            'technician_reset_password' => "【貂蝉到家】您正在进行技师重置密码操作, 验证码为{$code},请勿泄漏于他人。",
            'technician_order'          => "【貂蝉到家】{$time}您有新的订单.请登录app查看",
            'user_address'              => "【貂蝉到家】您的验证码{$code}，该验证码10分钟内有效，请勿泄漏于他人！",
        ];

        #模板code不存在
        if (empty($template_code_info[$template_code])) {
            \think\facade\Log::error('template_code 不存在！');
            throw new Exception('template_code 不存在！');
        }


        Db::startTrans();
        try {
            Db::table('ba_sms')->insert([
                'event'        => $template_code,
                'mobile'       => $mobile,
                'code'         => $code,
                'expired_time' => time() + 60 * 10, //10分钟过期
            ]);

            $sms = new \sms\Index();
            $info = $sms->send([
                "phone"   => $mobile,
                'content' => $template_code_info[$template_code]
            ]);
            \think\facade\Log::info('sms_send info:' . json_encode($info));
            if ($info['code'] != 0) {
                \think\facade\Log::error('短信发送失败！' . json_encode($info));
                throw new Exception('短信发送失败！');
            }
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            \think\facade\Log::error('短信发送失败！' . $e->getMessage());
            throw new Exception('短信发送失败！');
        }
        return true;
    }
}

/**
 * 验证短信
 */
if (!function_exists('sms_check')) {
    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    function sms_check ($mobile, $code, $template_code): bool
    {
        $ret = Db::name('sms')->where([
            'event'  => $template_code,
            'code'   => $code,
            'mobile' => $mobile,
        ])->find();
        if (empty($ret)) {
            throw new Exception('验证码错误！');
        }

        if ($ret['expired_time'] < time()) {
            throw new Exception('验证码已过期！');
        }
        Db::name('sms')->where([
            'event'  => $template_code,
            'mobile' => $mobile,
        ])->delete();

        return true;
    }
}

/**
 * 计算距离
 */
if (!function_exists('distance')) {
    /**
     * @throws ModelNotFoundException
     * @throws DataNotFoundException
     * @throws DbException
     */
    function distance ($lat1, $lon1, $lat2, $lon2, $unit = "km"): float
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtolower($unit);
        if ($unit == "km") {
            return ($miles * 1.609344);
        } else if ($unit == "mi") {
            return $miles;
        } else {
            return $miles;
        }
    }
}