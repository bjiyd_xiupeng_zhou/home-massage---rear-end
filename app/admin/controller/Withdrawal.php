<?php

namespace app\admin\controller;

use app\api\model\TechnicianMoneyLog;
use app\common\controller\Backend;
use think\Exception;
use think\facade\Db;
use think\facade\Log;

/**
 *
 */
class Withdrawal extends Backend
{
    /**
     * Withdrawal模型对象
     * @var object
     * @phpstan-var \app\admin\model\Withdrawal
     */
    protected object $model;

    protected string|array $defaultSortField = 'withdrawal_id,desc';

    protected array|string $preExcludeFields = ['create_time'];

    protected string|array $quickSearchField = ['withdrawal_id'];

    protected array $withJoinTable = ['technician', 'card'];

    public function initialize (): void
    {
        parent::initialize();
        $this->model = new \app\admin\model\Withdrawal;
    }


    /**
     * 若需重写查看、编辑、删除等方法，请复制 @see \app\admin\library\traits\Backend 中对应的方法至此进行重写
     */


    /**
     * 查看
     * @throws Throwable
     */
    public function index (): void
    {
        if ($this->request->param('select')) {
            $this->select();
        }

        list($where, $alias, $limit, $order) = $this->queryBuilder();
        $res = $this->model
            ->field($this->indexField)
            ->withJoin($this->withJoinTable, "LEFT")
            ->alias($alias)
            ->where($where)
            ->order($order)
            ->paginate($limit);

        $this->success('', [
            'list'   => $res->items(),
            'total'  => $res->total(),
            'remark' => get_route_remark(),
        ]);
    }


    /**
     * 设置已付款
     * @return void
     */
    public function payment (): void
    {
        $id = $this->request->get('id');
        $ret = $this->model->where('withdrawal_id', $id)->update(['status' => 1]);
        if (!$ret) {
            $this->error('设置失败');
        }
        $this->success('', $ret);
    }


    /**
     * 驳回提现
     * @return void
     */
    public function reject (): void
    {
        #开启事务
        Db::startTrans();
        try {
            $id = $this->request->get('id');
            $msg = $this->request->get('msg');
            $ret = $this->model->where('withdrawal_id', $id)->find();
            $ret->status = 2;
            $ret->msg = $msg;
            $ret->save();

            #将金额退回到技师余额
            $technician_id = $ret->technician_id;
            $technicianInfo = \app\admin\model\Technician::where('technician_id', $technician_id)->find();
            $technicianInfo->money = bcadd($technicianInfo->money, $ret->price, 2);
            $technicianInfo->save();

            #记录到技师余额变动表
            $TechnicianMoneyLog = new TechnicianMoneyLog();
            $TechnicianMoneyLogInfo = $TechnicianMoneyLog->where('technician_id', $technician_id)->order('create_time', 'desc')->find();
            $TechnicianMoneyLog->technician_id = $technician_id;
            $TechnicianMoneyLog->money = $ret->price;
            $TechnicianMoneyLog->before = $TechnicianMoneyLogInfo->after;
            $TechnicianMoneyLog->after = bcadd($TechnicianMoneyLogInfo->after, $ret->price, 2);
            $TechnicianMoneyLog->type = 6;
            $TechnicianMoneyLog->memo = '提现驳回 ' . $msg;
            $TechnicianMoneyLog->create_time = date('Y-m-d H:i:s', time());
            $TechnicianMoneyLog->save();
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            Log::error('提现驳回失败- ' . $e->getMessage());
            $this->error('驳回失败');
        }
        $this->success('驳回成功');
    }

}