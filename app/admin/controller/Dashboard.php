<?php

namespace app\admin\controller;

use app\common\controller\Backend;

class Dashboard extends Backend
{
    public function initialize (): void
    {
        parent::initialize();
    }

    public function index (): void
    {
        $toDayOrderTotal = \app\admin\model\Order::whereTime(
            'create_time', 'today'
        )->count('id');

        $toDayOrderPriceTotal = \app\admin\model\Order::whereTime(
            'create_time', 'today'
        )->where('order_state', 'Completed')->count('id');

        $toDayUserTotal = \app\admin\model\User::whereTime(
            'create_time', 'today'
        )->count('id');

        $technicianTotal = \app\admin\model\Technician::count('technician_id');
        $technicianIsOnGoWorkTotal = \app\admin\model\Technician::where('is_go_work', 1)->count('technician_id');
        $technicianIsOutGoWorkTotal = \app\admin\model\Technician::where('is_go_work', 2)->count('technician_id');


        $this->success('', [
            'toDayOrderTotal'            => $toDayOrderTotal,
            'toDayOrderPriceTotal'       => $toDayOrderPriceTotal,
            'toDayUserTotal'             => $toDayUserTotal,
            'technicianTotal'            => $technicianTotal,
            'technicianIsOnGoWorkTotal'  => $technicianIsOnGoWorkTotal,
            'technicianIsOutGoWorkTotal' => $technicianIsOutGoWorkTotal,
            'remark'                     => get_route_remark()
        ]);
    }
}