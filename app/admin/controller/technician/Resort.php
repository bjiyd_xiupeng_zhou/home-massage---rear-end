<?php

namespace app\admin\controller\technician;

use app\admin\model\Technician;
use app\common\controller\Backend;
use Throwable;

/**
 * 技师求助通知管理
 */
class Resort extends Backend
{
    /**
     * Resort模型对象
     * @var object
     * @phpstan-var \app\admin\model\technician\Resort
     */
    protected object $model;

    protected array|string $preExcludeFields = ['id', 'create_time'];

    protected string|array $quickSearchField = ['id'];

    public function initialize (): void
    {
        parent::initialize();
        $this->model = new \app\admin\model\technician\Resort;
    }


    /**
     * 若需重写查看、编辑、删除等方法，请复制 @see \app\admin\library\traits\Backend 中对应的方法至此进行重写
     */

    /**
     * 查看
     * @throws Throwable
     */
    public function index (): void
    {
        if ($this->request->param('select')) {
            $this->select();
        }

        list($where, $alias, $limit, $order) = $this->queryBuilder();

        $model = $this->model->field($this->indexField)
            ->withJoin($this->withJoinTable, $this->withJoinType)
            ->alias($alias)
            ->where($where)
            ->order($order);

        $isAgentWhere = $this->isAgentWhere();
        if (!empty($isAgentWhere)) {
            $technicianIds = Technician::where($isAgentWhere)->column('technician_id');
            $model->whereIn('technician_id', $technicianIds);
        }
        $ret = $model->paginate($limit);

        $this->success('', [
            'getLastSql' => $model->getLastSql(),
            'asd'        => $technicianIds ?? [],
            'list'       => $ret->items(),
            'total'      => $ret->total(),
            'remark'     => get_route_remark(),
        ]);
    }


    public function read ()
    {
        $id = $this->request->param('id');
        $res = $this->model->where('id', $id)->find();
        if (!$res) {
            $this->error('数据不存在');
        }
        $res->is_read = 1;
        if (!$res->save()) {
            $this->error('设置失败');
        }
        $this->success('设置成功');
    }
}