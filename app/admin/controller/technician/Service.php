<?php

namespace app\admin\controller\technician;

use app\common\controller\Backend;
use Throwable;

/**
 * 技师服务关联管理
 */
class Service extends Backend
{
    /**
     * Service模型对象
     * @var object
     * @phpstan-var \app\admin\model\technician\Service
     */
    protected object $model;

    protected array|string $preExcludeFields = ['id'];

    protected string|array $quickSearchField = ['id'];

    public function initialize (): void
    {
        parent::initialize();
        $this->model = new \app\admin\model\technician\Service;
    }


    /**
     * 若需重写查看、编辑、删除等方法，请复制 @see \app\admin\library\traits\Backend 中对应的方法至此进行重写
     */


    /**
     * 添加
     */
    public function add (): void
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            if (!$data) {
                $this->error(__('Parameter %s can not be empty', ['']));
            }

            $data = $this->excludeFields($data);
            if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                $data[$this->dataLimitField] = $this->auth->id;
            }

            $result = false;
            $this->model->startTrans();
            try {
                // 模型验证
                if ($this->modelValidate) {
                    $validate = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                    if (class_exists($validate)) {
                        $validate = new $validate;
                        if ($this->modelSceneValidate) $validate->scene('add');
                        $validate->check($data);
                    }
                }

                if (empty($data['service_ids'])) {
                    $this->error('请选择关联的服务');
                }
                if (empty($data['technician_id'])) {
                    $this->error('请选择关联的技师');
                }
                $insert = [];
                foreach ($data['service_ids'] as $key => $value) {
                    $insert[$key]['service_id'] = $value;
                    $insert[$key]['technician_id'] = $data['technician_id'];
                }
                $result = $this->model->saveAll($insert);
                $this->model->commit();
            } catch (Throwable $e) {
                $this->model->rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success(__('Added successfully'));
            } else {
                $this->error(__('No rows were added'));
            }
        }

        $this->error(__('Parameter error'));
    }


    /**
     * 删除
     *
     * @throws Throwable
     */
    public function del (array $ids = []): void
    {

        $technician_id = $this->request->param('technician_id');
        $service_id = $this->request->param('service_id');


        $this->model->startTrans();
        try {

            $this->model->where('service_id', $service_id)
                ->where('technician_id', $technician_id)->delete();

            $this->model->commit();
        } catch (Throwable $e) {
            $this->model->rollback();
            $this->error($e->getMessage());
        }

        $this->success(__('Deleted successfully'));

    }
}