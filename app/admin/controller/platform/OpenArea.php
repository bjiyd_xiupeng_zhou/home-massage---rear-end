<?php

namespace app\admin\controller\platform;

use app\common\controller\Backend;

/**
 * 平台开放地区管理
 */
class OpenArea extends Backend
{
    /**
     * OpenArea模型对象
     * @var object
     * @phpstan-var \app\admin\model\platform\OpenArea
     */
    protected object $model;

    protected array|string $preExcludeFields = ['id', 'create_time'];

    protected string|array $quickSearchField = ['id'];
    protected array $withJoinTable = ['area'];

    public function initialize (): void
    {
        parent::initialize();
        $this->model = new \app\admin\model\platform\OpenArea;
    }


    /**
     * 若需重写查看、编辑、删除等方法，请复制 @see \app\admin\library\traits\Backend 中对应的方法至此进行重写
     */


    /**
     * 查看
     * @throws Throwable
     */
    public function index (): void
    {
        if ($this->request->param('select')) {
            $this->select();
        }

        list($where, $alias, $limit, $order) = $this->queryBuilder();
        $res = $this->model
            ->field($this->indexField)
            ->withJoin($this->withJoinTable, $this->withJoinType)
            ->alias($alias)
            ->where($where)
            ->order($order)
            ->paginate($limit)->each(function ($item) {
                $item->city_name = $item->area->name;
                return $item;
            });

        $this->success('', [
            'list'   => $res->items(),
            'total'  => $res->total(),
            'remark' => get_route_remark(),
        ]);
    }
}