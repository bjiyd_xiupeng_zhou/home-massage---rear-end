<?php

namespace app\admin\controller\service;

use app\common\controller\Backend;
use Throwable;

/**
 * 服务分类
 */
class Classification extends Backend
{
    /**
     * Classification模型对象
     * @var object
     * @phpstan-var \app\admin\model\service\Classification
     */
    protected object $model;

    protected string|array $defaultSortField = 'class_id,desc';

    protected array|string $preExcludeFields = ['create_time'];

    protected string|array $quickSearchField = ['class_id'];

    protected array $withJoinTable = ['admin'];

    public function initialize (): void
    {
        parent::initialize();
        $this->model = new \app\admin\model\service\Classification;
    }


    /**
     * 若需重写查看、编辑、删除等方法，请复制 @see \app\admin\library\traits\Backend 中对应的方法至此进行重写
     */


    /**
     * 查看
     * @throws Throwable
     */
    public function index (): void
    {
        if ($this->request->param('select')) {
            $this->select();
        }

        list($where, $alias, $limit, $order) = $this->queryBuilder();
        $res = $this->model
            ->field($this->indexField)
//            ->withJoin($this->withJoinTable, $this->withJoinType)
            ->where($this->isAgentWhere())
            ->alias($alias)
            ->where($where)
            ->order($order)
            ->paginate($limit);

        $this->success('', [
            'list'   => $res->items(),
            'total'  => $res->total(),
            'remark' => get_route_remark(),
        ]);
    }


    /**
     * 添加
     */
    public function add (): void
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            if (!$data) {
                $this->error(__('Parameter %s can not be empty', ['']));
            }

            $data = $this->excludeFields($data);
            if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                $data[$this->dataLimitField] = $this->auth->id;
            }

            $result = false;
            $this->model->startTrans();
            try {
                // 模型验证
                if ($this->modelValidate) {
                    $validate = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                    if (class_exists($validate)) {
                        $validate = new $validate;
                        if ($this->modelSceneValidate) $validate->scene('add');
                        $validate->check($data);
                    }
                }

                $data['ascription_id'] = $this->ascription_id();
                $result = $this->model->save($data);
                $this->model->commit();
            } catch (Throwable $e) {
                $this->model->rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success(__('Added successfully'));
            } else {
                $this->error(__('No rows were added'));
            }
        }

        $this->error(__('Parameter error'));
    }

}