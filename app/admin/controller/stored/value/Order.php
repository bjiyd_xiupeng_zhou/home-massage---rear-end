<?php

namespace app\admin\controller\stored\value;

use app\common\controller\Backend;

/**
 * 储值订单
 */
class Order extends Backend
{
    /**
     * Order模型对象
     * @var object
     * @phpstan-var \app\admin\model\stored\value\Order
     */
    protected object $model;

    protected array|string $preExcludeFields = ['id', 'create_time'];
    protected array $withJoinTable = ['user', 'stored'];

    protected string|array $quickSearchField = ['id'];

    public function initialize (): void
    {
        parent::initialize();
        $this->model = new \app\admin\model\stored\value\Order;
    }


    /**
     * 若需重写查看、编辑、删除等方法，请复制 @see \app\admin\library\traits\Backend 中对应的方法至此进行重写
     */
}