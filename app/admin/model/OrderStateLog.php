<?php

namespace app\admin\model;
use think\Model;

class OrderStateLog extends  Model
{
    // 表名
    protected $name = 'order_state_timeline';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;
}