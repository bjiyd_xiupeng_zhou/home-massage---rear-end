<?php

namespace app\admin\model;

use think\Model;

/**
 * Classification
 */
class Classification extends Model
{
    // 表主键
    protected $pk = 'class_id';

    // 表名
    protected $name = 'service_classification';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;

}