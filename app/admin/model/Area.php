<?php

namespace app\admin\model;

use think\Model;

/**
 * Technician
 */
class Area extends Model
{
    // 表主键
    protected $pk = 'id';

    // 表名
    protected $name = 'area';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;


    public function getBriefIntroductionAttr($value): string
    {
        return !$value ? '' : htmlspecialchars_decode($value);
    }
}