<?php

namespace app\admin\model\service;

use think\Model;

/**
 * Classification
 */
class Classification extends Model
{
    // 表主键
    protected $pk = 'class_id';

    // 表名
    protected $name = 'service_classification';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;


//    public function admin ()
//    {
//        return $this->belongsTo(\app\admin\model\Admin::class, 'ascription_id', 'id');
//    }

    public function service(){
        return $this->hasMany(Service::class, 'class_id','class_id');
    }
}