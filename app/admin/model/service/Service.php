<?php

namespace app\admin\model\service;

use think\Model;

/**
 * Service
 */
class Service extends Model
{
    // 表主键
    protected $pk = 'service_id';

    // 表名
    protected $name = 'service';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;


    public function class(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(\app\admin\model\Classification::class, 'class_id', 'class_id');
    }

    public function getServiceCoverImgTextAttr($row, $rows)
    {
        if(empty($rows['service_cover_img'])){
            return "";
        }
        return IMAGE_URL. $rows['service_cover_img'];
    }
}