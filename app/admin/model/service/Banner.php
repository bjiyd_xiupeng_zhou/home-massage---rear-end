<?php

namespace app\admin\model\service;

use think\Model;

/**
 * Banner
 */
class Banner extends Model
{
    // 表名
    protected $name = 'banner';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;

}