<?php

namespace app\admin\model;

use think\Model;

/**
 * Withdrawal
 */
class Withdrawal extends Model
{
    // 表主键
    protected $pk = 'withdrawal_id';

    // 表名
    protected $name = 'withdrawal';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;


    public function technician ()
    {
        return $this->hasOne(Technician::class, 'technician_id', 'technician_id');
    }

    public function card ()
    {
        return $this->hasOne(Card::class, 'card_id', 'card_id');
    }
}