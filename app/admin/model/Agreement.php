<?php

namespace app\admin\model;

use think\Model;

/**
 * Agreement
 */
class Agreement extends Model
{
    // 表名
    protected $name = 'agreement';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;


    public function getContentAttr($value): string
    {
        return !$value ? '' : htmlspecialchars_decode($value);
    }
}