<?php

namespace app\admin\model\stored;

use think\Model;

/**
 * Value
 */
class Value extends Model
{
    // 表名
    protected $name = 'stored_value';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;

}