<?php

namespace app\admin\model\stored\value;

use app\admin\model\stored\Value;
use app\admin\model\User;
use think\Model;

/**
 * Order
 */
class Order extends Model
{
    // 表名
    protected $name = 'stored_value_order';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;


    public function user ()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function stored ()
    {
        return $this->belongsTo(Value::class, 'stored_id', 'id');
    }
}