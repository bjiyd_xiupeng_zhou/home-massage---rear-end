<?php

namespace app\admin\model\platform;

use think\Model;

/**
 * OpenArea
 */
class OpenArea extends Model
{
    // 表名
    protected $name = 'platform_area';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;


    public function area ()
    {
        return $this->belongsTo(\app\admin\model\Area::class, 'city_id', 'id');
    }

}