<?php

namespace app\admin\model;

use app\admin\model\service\Service;
use app\api\model\FareOrder;
use app\api\services\Refund;
use app\common\model\RefundOrder;
use think\Model;

/**
 * Order
 */
class Order extends Model
{
    // 表名
    protected $name = 'order';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;

    public function user ()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function service ()
    {
        return $this->belongsTo(Service::class, 'service_id', 'service_id');
    }

    public function technician ()
    {
        return $this->belongsTo(Technician::class, 'technician_id', 'technician_id');
    }

    public function fareorder ()
    {
        return $this->belongsTo(\app\technician_api\model\Fareorder::class, 'id', 'order_id');
    }

    public function getOrderStateTextAttr ($value, $data)
    {
        $arr = [
            'ToBePaid'  => '待支付',
            'Paid'      => '已支付',
            'Completed' => '已完成',
            'Canceled'  => '已取消',
            'SetOut'    => '技师已出发',
            'Start'     => '技师开始服务中',
            'Reach'     => '技师已到达，正在服务中',
            'Received'  => '技师已接单',
            'tchEnd'   => '待用户确认',
        ];
        return $arr[$data['order_state']];
    }

    public function refund ()
    {
        return $this->hasOne(RefundOrder::class, 'order_id', 'id');
    }

    public function fare ()
    {
        return $this->hasOne(FareOrder::class, 'order_id', 'id');
    }
}