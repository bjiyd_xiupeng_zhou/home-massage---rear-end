<?php

namespace app\admin\model\technician;

use think\Model;

/**
 * Service
 */
class Service extends Model
{
    // 表名
    protected $name = 'technician_service';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;


    public function service()
    {
        return $this->hasOne(\app\admin\model\service\Service::class,'service_id','service_id');
    }
    public function technician()
    {
        return $this->hasOne(\app\admin\model\Technician::class,'technician_id','technician_id');
    }
}