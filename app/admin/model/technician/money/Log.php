<?php

namespace app\admin\model\technician\money;

use app\admin\model\Technician;
use think\Model;

/**
 * Log
 */
class Log extends Model
{
    // 表名
    protected $name = 'technician_money_log';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;

    public function technician ()
    {
        return $this->hasOne(Technician::class, 'technician_id', 'technician_id')->field('technician_id,technician_name,tel');
    }
}