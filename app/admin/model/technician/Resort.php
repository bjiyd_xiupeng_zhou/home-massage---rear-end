<?php

namespace app\admin\model\technician;

use think\Model;

/**
 * Resort
 */
class Resort extends Model
{
    // 表名
    protected $name = 'technician_resort';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;
    protected $updateTime = false;

}