export default {
    id: "ID",
    stored_id: '套餐id',
    user_id: '用户id',
    order_sn: '订单号',
    pay_sn: '支付订单号',
    create_time: '下单时间',
    pay_time: '支付时间',
    state: '状态',
    pay_type: '支付类型',
    'quick Search Fields': 'id',
}
