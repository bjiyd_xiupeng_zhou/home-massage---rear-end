export default {
    id: 'ID',
    package_name: '套餐名称',
    payment_price:
        '支付金额',
    received_amount:
        '到账金额',
    weight:
        '权重',
    status:
        '是否上架',
    create_time:
        '创建时间', type:
        '类型',
    'quick Search Fields':
        'id',
}
