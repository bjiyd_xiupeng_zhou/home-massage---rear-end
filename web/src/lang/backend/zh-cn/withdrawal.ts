export default {
    withdrawal_id: 'ID',
    price: '提现金额',
    card_id: '银行卡ID',
    create_time: '创建时间',
    technician_id: '技师ID',
    status: '状态',
    'status 0': '申请中',
    'status 1': '已打款',
    'status 2': '已拒绝打款',
    msg: '拒绝打款原因',
    'quick Search Fields': 'ID',
}
