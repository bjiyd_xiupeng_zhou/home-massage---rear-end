export default {
    technician_id: '技师id',
    technician_name: '技师姓名',
    tel: '手机号',
    address: '求救地址',
    lng: '经度',
    lat: '维度',
    is_read: '已读状态 0未读 1已读',
    create_time: '求助时间',
    read_time: '已读时间',
    'quick Search Fields': 'id',
}
