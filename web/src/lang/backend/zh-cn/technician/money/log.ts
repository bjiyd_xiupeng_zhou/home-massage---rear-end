export default {
    id: 'ID',
    technician_id: '技师ID',
    money: '变更余额',
    before: '变更前余额',
    after: '变更后余额',
    memo: '备注',
    create_time: '变更时间',
    'quick Search Fields': 'ID',
}
