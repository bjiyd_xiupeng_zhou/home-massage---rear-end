export default {
    withdrawal_id: 'withdrawal_id',
    price: 'price',
    card_id: 'card_id',
    create_time: 'create_time',
    technician_id: 'technician_id',
    status: 'status',
    'status 0': 'status 0',
    'status 1': 'status 1',
    'status 2': 'status 2',
    msg: 'msg',
    'quick Search Fields': 'withdrawal_id',
}
