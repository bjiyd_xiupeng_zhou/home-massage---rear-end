export default {
    class_id: 'class_id',
    class_name: 'class_name',
    class_weight: 'class_weight',
    create_time: 'create_time',
    class_ascription_id: 'class_ascription_id',
    class_status: 'class_status',
    'quick Search Fields': 'class_id',
}
