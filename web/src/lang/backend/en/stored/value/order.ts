export default {
    stored_id: 'stored_id',
    user_id: 'user_id',
    order_sn: 'order_sn',
    pay_sn: 'pay_sn',
    create_time: 'create_time',
    pay_time: 'pay_time',
    state: 'state',
    pay_type: 'pay_type',
    'quick Search Fields': 'id',
}
