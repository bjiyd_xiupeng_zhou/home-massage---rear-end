export default {
    technician_id: 'technician_id',
    technician_name: 'technician_name',
    tel: 'tel',
    address: 'address',
    lng: 'lng',
    lat: 'lat',
    is_read: 'is_read',
    create_time: 'create_time',
    read_time: 'read_time',
    'quick Search Fields': 'id',
}
