import createAxios from '/@/utils/axios'

export function payment(data: Object) {
    return createAxios({
        url: '/admin/withdrawal/payment',
        method: 'get',
        params: data
    })
}


export function reject(data: Object) {
    return createAxios({
        url: '/admin/withdrawal/reject',
        method: 'get',
        params: data
    })
}

