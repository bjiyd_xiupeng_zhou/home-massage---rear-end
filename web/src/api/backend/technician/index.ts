import createAxios from '/@/utils/axios'

export function technicianServiceAdd(data: Object) {
    return createAxios({
        url: '/admin/technician.service/add',
        method: 'post',
        data: data
    })
}


export function technicianServiceSel(data: Object) {
    return createAxios({
        url: '/admin/service.Service/index2',
        method: 'get',
        params: data
    })
}

export function cancelService(data: Object) {
    return createAxios({
        url: '/admin/technician.Service/del',
        method: 'get',
        params: data
    })
}

/**
 * 设置求救通知已读
 * @param data
 */
export function setRead(data: Object) {
    return createAxios({
        url: '/admin/technician.Resort/read',
        method: 'get',
        params: data
    })
}

/**
 * 获取服务记录
 * @param data
 */
export function getServiceLog(data: Object) {
    return createAxios({
        url: '/admin/Technician/serviceLog',
        method: 'get',
        params: data
    })
}

/**
 * 获取评价记录
 * @param data
 */
export function getEvaluateLog(data: Object) {
    return createAxios({
        url: '/admin/Technician/getEvaluateLog',
        method: 'get',
        params: data
    })
}

/**
 * 获取技师余额变动记录
 * @param data
 */
export function getBalanceEditLog(data: Object) {
    return createAxios({
        url: '/admin/Technician/balanceEditLog',
        method: 'get',
        params: data
    })
}


/**
 * 修改余额
 * @param data
 */
export function addBalanceLog(data: Object) {
    return createAxios({
        url: '/admin/Technician/addBalanceLog',
        method: 'post',
        data: data
    })
}
