import createAxios from '/@/utils/axios'

export function cancelOrder(data: Object) {
    return createAxios({
        url: '/admin/order/cancelOrder',
        method: 'get',
        params: data
    })
}

export function getTimeline(data: Object) {
    return createAxios({
        url: '/admin/order/getTimeline',
        method: 'get',
        params: data
    })
}

export function getAddClockOrder(data: Object) {
    return createAxios({
        url: '/admin/order/getAddClockOrder',
        method: 'get',
        params: data
    })
}
export function getFareOrder(data: Object) {
    return createAxios({
        url: '/admin/order/getFareOrder',
        method: 'get',
        params: data
    })
}




