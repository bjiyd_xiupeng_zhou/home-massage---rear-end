import createAxios from '/@/utils/axios'

export const url = '/admin/user.user/'

export function log(param: Object) {
    return createAxios({
        url: url + 'log',
        method: 'get',
        params: param
    })
}
