import createAxios from '/@/utils/axios'

export function getAdminRules() {
    return createAxios({
        url: '/admin/auth.Rule/index',
        method: 'get',
    })
}

export function getIndexCity() {
    return createAxios({
        url: '/admin/platform.Area/index_city',
        method: 'get',
    })
}
