<?php
// +----------------------------------------------------------------------
// | 支付配置
// | #alipay# 和 #wechat# 这类标记是因为 key 可能重复，使用标记实现编程式修改配置
// +----------------------------------------------------------------------

return [
    'alipay' => [
        'default' => [
            'app_id'                  => '', #alipay#
            'app_secret_cert'         => '', #alipay#
            'app_public_cert_path'    => '', #alipay#
            'alipay_public_cert_path' => '', #alipay#
            'alipay_root_cert_path'   => '', #alipay#
            'return_url'              => '', #alipay#
            'notify_url'              => '', #alipay#
            'app_auth_token'          => '', #alipay#
            'service_provider_id'     => '', #alipay#
            'mode'                    => '0', #alipay#
        ]
    ],
    'wechat' => [
        'default' => [
            'mch_id'                  => '1503672131', #wechat#
            'mch_secret_key_v2'       => 'sdkm2020shandongkuma2020SDKM2020', #wechat#
            'mch_secret_key'          => 'sdkm2020shandongkuma2020SDKM2021', #wechat#
            'mch_secret_cert'         => 'extend/cert/apiclient_key.pem', #wechat#
            'mch_public_cert_path'    => 'extend/cert/apiclient_cert.pem', #wechat#
            'notify_url'              => 'https://eyeh5.sdkuma.com/api/PayNotify/wechat', #wechat#
            'mp_app_id'               => 'wxdd7c5002fade7975', #wechat#
            'mini_app_id'             => '', #wechat#
            'app_id'                  => '', #wechat#
            'combine_app_id'          => '', #wechat#
            'combine_mch_id'          => '', #wechat#
            'sub_mp_app_id'           => '', #wechat#
            'sub_app_id'              => '', #wechat#
            'sub_mini_app_id'         => '', #wechat#
            'sub_mch_id'              => '', #wechat#
            'wechat_public_cert_path' => '[{"key":"RSA","value":"534DCDDD72212470E26D35DE4203AC75C53A1848"}]', #wechat#
            'mode'                    => '0', #wechat#
        ]
    ],
    'unipay' => [
        'default' => [
            // 必填-商户号
            'mch_id'                  => '',
            // 必填-商户公私钥
            'mch_cert_path'           => '',
            // 必填-商户公私钥密码
            'mch_cert_password'       => '',
            // 必填-银联公钥证书路径
            'unipay_public_cert_path' => '',
            // 必填
            'return_url'              => '',
            // 必填
            'notify_url'              => '',
        ],
    ],
    'logger' => [
        'enable'   => false, #other#
        'file'     => 'runtime/pay/pay.log', #other#
        'level'    => 'debug', #other#
        'type'     => 'daily', #other#
        'max_file' => 30, #other#
    ],
    'http'   => [
        'timeout'         => 10, #other#
        'connect_timeout' => 10, #other#
        // 更多配置项请参考 [Guzzle](https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html)
    ],
];