<?php

namespace sms;

use think\facade\Log;

/**
 * 登录网址：http://119.23.241.143/sms/login.html
 * 账号：000601
 * 密码：CgJpBsFuNTeI
 *
 * 有20测试条数
 *
 * --------------- HTTP短信接口 ---------------
 * 接口网关地址： http://119.23.241.143:8001/sms
 * 用户帐号： 000601
 * 接口密码： xwAzaRAHsQNN
 * 绑定IP： 120.220.19.3
 */
class Index
{
    protected $username = "000601";
    // protected $password = "CgJpBsFuNTeI";
    protected $password = "xwAzaRAHsQNN";

    protected $time;

    public function __construct ()
    {

    }

    /**
     *
     * $info 内容
     * phone    String    是    发送手机号码
     * content    String    是    短信内容
     * extcode    String    否    可选，附带通道扩展码
     * callData    String    否    用户回传数据，最大长度64。
     * 用户若传递此参数将在回执推送时回传给用户。
     */
    public function send (array $info)
    {
//      测试加签
//         $this->username = "test";
//         $this->password = "123";
        // $this->time = "1596254400000";
//         echo $this->sign();
// die;

        $this->time = time() * 1000;

        $header = [
            CURLOPT_HTTPHEADER => [
                "Content-Type: application/json;charset=utf-8",
                "Accept: application/json",
            ]
        ];
        Log::record("处理短信");
        $response = $this->send_post("http://119.23.241.143:8001/sms/api/sendMessageOne", json_encode([$info], JSON_UNESCAPED_UNICODE), $header);


        Log::record($response);
        // $response = '{"code":0,"message":"处理成功","data":[{"code":0,"message":"处理成功","msgId":19378460373,"phone":"17686393630","smsCount":1}],"smsCount":1}';
        $response = json_decode($response, true);
        return $response;

    }


    protected function sign (): string
    {
        return MD5($this->username . ($this->time) . MD5($this->password));
    }

    protected function send_post ($url, $data, $option = [])
    {
        $curl = curl_init();

        curl_setopt_array($curl, array (
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => '',
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => 'POST',
            CURLOPT_POSTFIELDS     => '{
            "userName": "000601",
            "messageList": ' . $data . ',
            "timestamp": ' . $this->time . ',
            "sign": "' . $this->sign() . '"
        }',
            CURLOPT_HTTPHEADER     => array (
                'Accept: application/json',
                'Content-Type: application/json;charset=utf-8',
            ),
        ));
        // halt('{
        //     "userName": "000601",
        //     "messageList": '.$data.',
        //     "timestamp": '.$this->time.',
        //     "sign": "'.$this->sign().'"
        // }');
        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

}
