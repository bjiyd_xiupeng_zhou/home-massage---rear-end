<?php

namespace local;
// 天地图封装
class Area
{

    private static string  $key = '52365404002b546eac7ca494f98f074d';

    /**
     * 根据经纬度查询地址信息
     */
    public static function lnglat($lng, $lat)
    {
        return self::http("http://api.tianditu.gov.cn/geocoder?postStr={'lon':{$lng},'lat':{$lat},'ver':1}&type=geocode&tk=".self::$key);
    }


    private static function http($url){
        $ch = curl_init($url); // 初始化 cURL 会话
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // 将返回的数据以字符串形式返回而不是直接输出
        $response = curl_exec($ch); // 执行请求并获取响应数据
        curl_close($ch); // 关闭 cURL 会话
        return json_decode($response,true);
    }
}